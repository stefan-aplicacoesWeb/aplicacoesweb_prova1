/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

/**
 *
 * @author stefan
 */
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

//@ManagedBean(eager = true)
@Named
@ApplicationScoped
public class ApplicationContainer {

    private final ArrayList<LoginBean> logins = new ArrayList<>();

    public ApplicationContainer() {

    }

    public void addLogin(LoginBean bean) {
        logins.add(new LoginBean(bean));
    }
    
    public int getSize(){
        return logins.size();
    }

    public LoginBean[] getLogins() {
        return logins.toArray(new LoginBean[logins.size()]);
    }

}
